import json
import os
import sys

import pika
from datetime import datetime
import clickhouse_connect
from pymongo import MongoClient
import logging

import config


def get_database():
    # Provide the mongodb atlas url to connect python to mongodb using pymongo
    CONNECTION_STRING = f"mongodb://{config.MONGO_USER}:{config.MONGO_PASSWORD}@{config.MONGO_HOST}:27017/{config.MONGO_DBNAME}?authMechanism=DEFAULT&authSource=admin"

    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    client = MongoClient(CONNECTION_STRING)

    # Create the database for our example (we will use the same database throughout the tutorial
    return client[config.MONGO_DBNAME]


class Events:
    routing_keys = ['client.products.view', 'client.products.order', 'client.products.comp', 'client.categories.view']
    ProductsPageView = 0
    ProductsOrder = 1
    ProductsComp = 2
    CategoriesView = 3


def add_to_clickhouse(client, table_name, values):
    res = client.insert(table_name, [list(values.values())], column_names=list(values.keys()))
    return res


def add_to_mongodb(current_db, table_name, values):
    collection = current_db[table_name]
    insertion_res = collection.insert_one(values)
    return insertion_res


def main():
    client = clickhouse_connect.get_client(host=config.CLICKHOUSE_HOST, username=config.CLICKHOUSE_USER, password=config.CLICKHOUSE_PASSWORD)
    mongodb = get_database()

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=config.RABBIT_HOST))
    channel = connection.channel()

    channel.exchange_declare(exchange='topic_client_data', exchange_type='topic')

    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue

    binding_keys = Events.routing_keys

    for binding_key in binding_keys:
        channel.queue_bind(
            exchange='topic_client_data', queue=queue_name, routing_key=binding_key)
        logging.info(f"[] Queue bound to key {binding_key}")

    logging.info(' [*] Waiting for events. To exit press CTRL+C')

    def callback(ch, method, properties, body):
        table_name = method.routing_key.replace(".", "_")
        values = json.loads(body)
        for k in values.keys():
            if k == 'datetime':
                values[k] = datetime.fromisoformat(values[k])
            else:
                values[k] = int(values[k])
        try:
            logging.info(add_to_clickhouse(client, table_name, values))
            logging.info(add_to_mongodb(mongodb, table_name, values))
        except Exception as e:
            logging.error(e)

    channel.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True)

    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        logging.error('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
