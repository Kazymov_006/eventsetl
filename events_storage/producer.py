import pika
import sys
import json
import random
from datetime import datetime, timedelta


class Events:
    routing_keys = ['client.products.view', 'client.products.order', 'client.products.comp', 'client.categories.view']
    ProductsPageView = 0
    ProductsOrder = 1
    ProductsComp = 2
    CategoriesView = 3


def send_message(message, routing_key, channel):
    channel.basic_publish(
        exchange='topic_client_data', routing_key=routing_key, body=json.dumps(message))
    print(f" [x] Sent {routing_key}:{message}")


def seng_messages(messages, routing_key):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange='topic_client_data', exchange_type='topic')
    for message in messages:
        send_message(message, routing_key, channel)
    connection.close()



def random_datetime(start, end):
    return start + (end - start) * random.random()


def _generate_ppv_msg():
    case = random.randint(1,3)
    if case == 1:
        pid = random.randint(1, 100)
    elif case == 2:
        pid = random.randint(26, 75)
    elif case == 3:
        pid = random.randint(38, 62)
    msg = {
        "product_id": str(pid),
        "user_id": str(random.randint(1,1000)),
        "datetime": random_datetime(datetime.now(), datetime.now() - timedelta(weeks=1)).isoformat()
    }
    return msg


def _generate_po_msg():
    case = random.randint(1,3)
    if case == 1:
        pid = random.randint(1, 100)
    elif case == 2:
        pid = random.randint(26, 75)
    elif case == 3:
        pid = random.randint(38, 62)
    msg = {
        "product_id": str(pid),
        "user_id": str(random.randint(1,1000)),
        "category_id": str((pid + 9) // 10),
        "datetime": random_datetime(datetime.now(), datetime.now() - timedelta(weeks=1)).isoformat()
    }
    return msg


def _generate_pc_msg():
    case = random.randint(1,3)
    if case == 1:
        pid = random.randint(1, 100)
    elif case == 2:
        pid = random.randint(26, 75)
    elif case == 3:
        pid = random.randint(38, 62)
    pid2 = random.randint(1, 100)
    while pid2 == pid:
        pid2 = random.randint(1, 100)
    msg = {
        "product1_id": str(pid2),
        "product2_id": str(pid),
        "user_id": str(random.randint(1,1000)),
        "datetime": random_datetime(datetime.now(), datetime.now() - timedelta(weeks=1)).isoformat()
    }
    return msg


def _generate_cv_msg():
    case = random.randint(1,3)
    if case == 1:
        pid = random.randint(1, 100)
    elif case == 2:
        pid = random.randint(26, 75)
    elif case == 3:
        pid = random.randint(38, 62)
    msg = {
        "category_id": str((pid + 9) // 10),
        "user_id": str(random.randint(1,1000)),
        "datetime": random_datetime(datetime.now(), datetime.now() - timedelta(weeks=1)).isoformat()
    }
    return msg


def generate_messages(num, event_type):
    messages = []
    if event_type == Events.ProductsPageView:
        for _ in range(num):
            messages.append(_generate_ppv_msg())
    elif event_type == Events.ProductsOrder:
        for _ in range(num):
            messages.append(_generate_po_msg())
    elif event_type == Events.ProductsComp:
        for _ in range(num):
            messages.append(_generate_pc_msg())
    elif event_type == Events.CategoriesView:
        for _ in range(num):
            messages.append(_generate_cv_msg())
    return messages
    

def main():
    event_type = Events.ProductsPageView
    routing_key = Events.routing_keys[event_type]

    messages = generate_messages(8000, event_type)
    seng_messages(messages, routing_key)
    del messages

    event_type = Events.ProductsOrder
    routing_key = Events.routing_keys[event_type]

    messages = generate_messages(4000, event_type)
    seng_messages(messages, routing_key)
    del messages

    event_type = Events.ProductsComp
    routing_key = Events.routing_keys[event_type]

    messages = generate_messages(2000, event_type)
    seng_messages(messages, routing_key)
    del messages

    event_type = Events.CategoriesView
    routing_key = Events.routing_keys[event_type]

    messages = generate_messages(10000, event_type)
    seng_messages(messages, routing_key)
    del messages


if __name__ == '__main__':
    main()
