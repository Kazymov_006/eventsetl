import clickhouse_connect

import config


def main():
    client = clickhouse_connect.get_client(host=config.CLICKHOUSE_HOST, username=config.CLICKHOUSE_USER,
                                           password=config.CLICKHOUSE_PASSWORD)
    client.command('CREATE TABLE IF NOT EXISTS client_products_view (product_id UInt32, user_id UInt32, datetime DateTime()) ENGINE MergeTree PRIMARY KEY (user_id, datetime)')
    client.command('CREATE TABLE IF NOT EXISTS client_products_order (product_id UInt32, user_id UInt32, category_id UInt32, datetime DateTime()) ENGINE MergeTree PRIMARY KEY (user_id, datetime)')
    client.command('CREATE TABLE IF NOT EXISTS client_products_comp (product1_id UInt32, product2_id UInt32, user_id UInt32, datetime DateTime()) ENGINE MergeTree PRIMARY KEY (user_id, datetime)')
    client.command('CREATE TABLE IF NOT EXISTS client_categories_view (category_id UInt32, user_id UInt32, datetime DateTime()) ENGINE MergeTree PRIMARY KEY (user_id, datetime)')


if __name__ == '__main__':
    main()
