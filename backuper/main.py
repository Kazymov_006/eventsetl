from bson.json_util import dumps
from pymongo import MongoClient
import json
from hdfs import InsecureClient
from datetime import date
import time

if __name__ == '__main__':
    hdfs_client = InsecureClient("http://namenode:9870", user="hadoop")
    collections = ['client_products_view', 'client_products_order', 'client_products_comp', 'client_categories_view']
    CONNECTION_STRING = f"mongodb://root:password@localhost:27017/marketplace?authMechanism=DEFAULT&authSource=admin"

    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    client = MongoClient(CONNECTION_STRING)
    db = client['marketplace']
    while True:
        for collection_name in collections:
            collection = db[collection_name]
            cursor = collection.find({})
            d = date.today()
            with hdfs_client.write(f'/users/hadoop/backups/{collection_name}_{d.year}_{d.month}_{d.day}.json', encoding='utf-8', overwrite=True) as writer:
                json.dump(json.loads(dumps(cursor)), writer)
            print(f"[x] backuped to /users/hadoop/backups/{collection_name}_{d.year}_{d.month}_{d.day}.json")
        time.sleep(60*60*24)
